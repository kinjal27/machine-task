import React, { useEffect, useState } from 'react';
import './homepage.css';
import { Button, Col, Container, Form, Row, Table } from 'react-bootstrap';
import axios from 'axios';

const Homepage = ({ updateUser }) => {
    const user = JSON.parse(localStorage.getItem('user'));

    const [userList, setUsers] = useState([]);
    const [fileList, setFileList] = useState([]);
    const [permittedUsers, setPermitterUsers] = useState(null);
    const [selectedFile, setSelectedFile] = useState('');

    const headers = {
        'authorization': `Bearer ${user.token}`,
    };

    useEffect(() => {
        const getUserList = async () => {
            const res = await axios.get('http://localhost:5000/v1/listUser', {headers});
            // res.data.users.unshift({_id: null, name: 'select user'})
            setUsers(res.data.users);
        }
        const getFileList = async () => {
            const res = await axios.get('http://localhost:5000/v1/listFile', {headers});
            setFileList(res.data.files);
        }
        getUserList();
        getFileList();
    }, []);

    const viewFile = async (fileId) => {
        try {
            const res = await axios.get(`http://localhost:5000/v1/fileDetail?userId=${user._id}&fileId=${fileId}`, headers);
            if (res.data.data) {
                var a = document.createElement("a");
                var file = new Blob([res.data.data]);
                a.href = URL.createObjectURL(file);
                a.download = fileList?.filter(value => value._id = fileId)[0]?.fileName;
                a.click();
            } else {
                alert('sorry!!, you do not have access')
            }
        } catch(err) {
            console.log(err)
        }
    }

    const selectUser = (e) => {
        setPermitterUsers(e.target.value)
    }

    const uploadFile = async (e) => {
        try {
            if (!permittedUsers) {
                if (userList && userList.length) setPermitterUsers(userList[0]._id);
                else {
                    alert('please select user to give permission');
                    e.preventDefault()
                    return;
                }
            }
            const formData = new FormData();
            formData.append('uploadFile', selectedFile)
            formData.append('permittedUsers', permittedUsers);
            const res = await axios.post('http://localhost:5000/v1/uploadFile', formData, { headers });
            if (res.status === 200) {
                var a = document.createElement("a");
                var file = new Blob([res.data.data]);
                a.href = URL.createObjectURL(file);
                a.download = res.data.fileName;
                a.click();
            } else {
                alert('something went wrong!!')
            }
        } catch(err) {
            console.log(err)
        }
    }

    return (
        <div className='homepage'>
            <Container className='container'>
                <Row>
                    <Col md={6}style={{ marginRight: "auto" }}>
                        <h1>Hello {user.name}!!</h1>
                    </Col>
                    <Col md={6}>
                        <Button className="pull-right" size="sm" type='button'
                          onClick={()=> updateUser({})}>
                            Logout
                        </Button>
                    </Col>
                </Row>
            </Container>
            <br></br>
            <Container className='uploadFile'>
            <Form onSubmit={uploadFile} encType='multipart/form-data'>
                <Row>
                    <Col>
                        <h4>
                            Upload File
                        </h4>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group controlId="formFile" className="mb-3">
                            <Form.Control type="file" name='uploadFile' onChange={(e) => setSelectedFile(e.target.files[0])} />
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Select onChange={selectUser}>
                        {
                            userList && userList.length ? 
                            userList.map((value) => (
                                <option key={value._id} value={value._id}>{value.name}</option>
                            ))
                            : 
                            <option disabled>no option availble</option>
                        }
                        </Form.Select>
                    </Col>
                    <Col>
                        <Button type='submit'>Upload</Button>
                    </Col>
                </Row>
                    </Form>
            </Container>
            <br></br>
            <Container className='homepage'>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                        <th>File Path</th>
                        <th>File Name</th>
                        <th>Uploaded By</th>
                        <th>View</th>
                        {/* <th>Edit</th> */}
                        </tr>
                    </thead>
                    <tbody>
                        { fileList && fileList.length ?
                            fileList.map((item) => (
                                <tr key={item.id}>
                                    <td>{item.filePath}</td>
                                    <td>{item.fileName}</td>
                                    <td>{item.uploadedBy}</td>
                                    <td><Button type='button' onClick={viewFile(item._id)}> View </Button></td>
                                    {/* <td>Edit</td> */}
                                    <td/>
                                </tr>
                            ))
                        : <tr>
                            <td colSpan={5} align='center'>
                                Please upload file
                            </td>
                        </tr> }
                    </tbody>
                </Table>
            </Container>
            <br></br>
        </div>
    )
}

export default Homepage;