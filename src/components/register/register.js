import React, { useState } from 'react';
import { Button, Container, Form } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

import './register.css';

const Register = ({ updateUser }) => {
    const history = useHistory();

    const [user, setUser] = useState({
        name: '',
        email: '',
        password: '',
        reEnterPassword: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUser({
            ...user,
            [name]: value
        });
    }

    const register = async (e) => {
        e.preventDefault();
        const { name, email, password, reEnterPassword } = user;
        if( name && email && password && reEnterPassword ) {
            if (password === reEnterPassword) {
                try {
                    const registerResponse = await axios.post('http://localhost:5000/register', user);
                    if (registerResponse && registerResponse.status && registerResponse.status === 200) {
                        updateUser(registerResponse.data.user);
                        history.push('/login');
                    } else {
                        alert(registerResponse.data.message);
                    }
                } catch(err) {
                    alert(err);
                }
            } else {
                alert('password not match');
            }
        } else {
            alert('invalid input');
        }
    }

    const login = () => {
        history.push('/login');
    }

    return (
        <Container className='register'>
            <Form>
                <Form.Group className="mb-3" controlId="formBasicName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" name='name' required
                    value={user.name} onChange={ handleChange } placeholder="Enter Name" />
                    <Form.Control.Feedback type="invalid">
                    Please enter name.
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" name='email' required
                    value={user.email} onChange={ handleChange } placeholder="Enter email" />
                    <Form.Control.Feedback type="invalid">
                    Please enter email.
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" name='password' required
                    value={user.password} onChange={ handleChange } placeholder="Password" />
                    <Form.Control.Feedback type="invalid">
                    Please enter password.
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicReEnterPassword">
                    <Form.Label>Re-enter Password</Form.Label>
                    <Form.Control type="password" name='reEnterPassword' required
                    value={user.reEnterPassword} onChange={ handleChange } placeholder="Re-enter Password" />
                    <Form.Control.Feedback type="invalid">
                    Please re-enter password.
                    </Form.Control.Feedback>
                </Form.Group>
                
                <Form.Group className="mb-3">
                    <Form.Label className='mt-3'>OR</Form.Label>
                </Form.Group>
                <Button variant="primary" type="submit" onClick={register}>
                    Register
                </Button>
                <Form.Group className="mb-3">
                    <Form.Label className='mt-3'>OR</Form.Label>
                </Form.Group>
                <Button variant="primary" type="button"  onClick={login}>
                    Login
                </Button>
            </Form>
        </Container>
    )
}

export default Register;