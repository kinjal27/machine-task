import React, { useState } from 'react';
import './login.css';
import { Button, Container, Form } from 'react-bootstrap';
import axios from 'axios';
import { useHistory } from 'react-router-dom';

const Login = ({ updateUser }) => {
    const history = useHistory();

    const [user, setUser] = useState({
        email: '',
        password: '',
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUser({
            ...user,
            [name]: value
        });
    }

    const login = async (e) => {
        e.preventDefault();
        const { email, password } = user;
        if (email && password) {
            try {
                const loginResponse = await axios.post('http://localhost:5000/login', user);
                console.log(loginResponse)
                if (loginResponse && loginResponse.data && loginResponse.data.user) {
                    updateUser(loginResponse.data.user);
                    history.push('/');
                } else {
                    alert(loginResponse.data.message);
                }
            } catch(err) {
                alert(err);
            }
        } else {
            alert('provide login details');
        }
    }

    const register = () => {
        history.push('/register');
    }

    return (
        <Container className='login'>
            <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" name='email' value={user.email}
                        onChange={handleChange} placeholder="Enter email" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" name='password' value={user.password}
                        onChange={handleChange} placeholder="Password" />
                </Form.Group>
                <Button variant="primary" type="submit" onClick={login}>
                    Login
                </Button>
                <Form.Group className="mb-3">
                    <Form.Label className='mt-3'>OR</Form.Label>
                </Form.Group>
                <Button variant="primary" type="submit"
                    onClick={register}>
                    Register
                </Button>
            </Form>
        </Container>
    )
}

export default Login;