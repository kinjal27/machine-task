import './App.css';
import {
  BrowserRouter as Router, Switch, Route
} from "react-router-dom";
import Homepage from './components/homepage/homepage';
import Login from './components/login/login';
import Register from './components/register/register';
import { useEffect, useState } from 'react';

function App() {
  const [user, setLoginUser] = useState({});

  useEffect(() => {
    setLoginUser(JSON.parse(localStorage.getItem('user')));
  } ,[])

  const updateUser = (user) => {
    localStorage.setItem('user', JSON.stringify(user));
    setLoginUser(user);
  }
  
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path='/'> {
            user && user._id ? 
              <Homepage updateUser={updateUser}/> : <Login  updateUser={updateUser} />
          }</Route>
          {/* <Route path='/login'>
          {
            user && user._id ? 
              <Homepage updateUser={updateUser}/> : <Login  updateUser={updateUser} />
          }</Route>
          <Route path='/register'>
          {
            user && user._id ?
            <Homepage updateUser={updateUser}/> : <Register updateUser={updateUser}/>
          }</Route> */}
        </Switch>
      </Router>
    </div>
  );
}

export default App;